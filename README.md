# Migrating from Fuse 1.x to Fuse 2.0 

This guide is for developers/publishers who have integrated with the legacy Fuse 1.x SDK and want to take advantage of all the great features of Fuse 2.0.

## New iOS Features

[TODO] ???

## Changes to iOS APIs

This section talks about existing features you may have integrated on iOS.

We review classes, protocols, and methods that you may be using in the legacy Fuse 1.38 SDK, discussing relevant details on changes and/or APIs that supercede legacy APIs from 1.38.

### FuseAdDelegate

This protocol no longer exists.

The following method interfaces have moved into the `FuseDelegate` protocol:

	-(void)adWillClose


### FuseDelegate

The following methods have changed:

#### -(void)sessionLoginError:

* The error type has changed: 

	`NSNumber` => `NSError`

#### -(void)rewardedVideoCompleted:

This method signature has been replaced by:

	-(void)rewardedAdCompleteWithObject:(FuseRewardedObject *)

* The parameter is now a `FuseRewardedObject`.

### FuseSDK

This class has been replaced by: `FuseAPI`.

The APIs are similar, but the following methods have changed:


#### +(void)startSession:Delegate:AutoRegisterForPush:

This method signature has been replaced by:

	`+(void)startSession:delegate:withOptions:`

* The auto-push registration is now an option. For example, the options for `NO` would be:

```
NSDictionary *options = @{kFuseSDKOptionKey_RegisterForPush: @NO};
```

#### +(void)showAdWithDelegate:

This method signature has been replaced by:

	`+(void)showAdWithZoneID:options:`

* You no longer pass the delegate directly. The delegate was registered via the startSession call.
* There are new options for the ad being shown, e.g. preroll/postroll for rewarded videos


#### +(void)preLoadAdForZone:

This method signature has been replaced by:

	`+(void)loadAdWithZoneID:`

[TODO] ??? The wiki docs say preLoadAdForZone is still available, but it's commented out in the header file.


#### +(void)checkAdAvailableWithDelegate:withAdZone:

This method signature has been replaced by:

	`+(BOOL)isAdAvailableForZoneID:`

* Unlike the old API, this new API is synchronous.


## New Android Features

[TODO] ???

## Changes to Android APIs

This section talks about existing features you may have integrated on Android.

We review classes, protocols, and methods that you may be using in the legacy Fuse 1.38 SDK, discussing relevant details on changes and/or APIs that supercede legacy APIs from 1.38.

[TODO] ???
